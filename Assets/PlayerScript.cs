﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
	public float maxSpeed = 1.0f;
	public float jumpForce = 5.0f;
	Transform feet;
	bool isGrounded = true;
	public float groundCheckRadius = 0.2f;
	public LayerMask groundLayer;
	// Use this for initialization
	void Start ()
	{
		feet = GameObject.FindGameObjectWithTag("Feet").GetComponent<Transform>();
	}
	
	// Update is called once per frame

	void Update ()
	{
		if (Input.GetButtonDown ("Jump")) {
			if (isGrounded) {
				Rigidbody2D rig = GetComponent<Rigidbody2D>();
				rig.velocity = new Vector2 (rig.velocity.x, jumpForce);
			}
		}

	}

	void FixedUpdate()
	{
		isGrounded = Physics2D.OverlapCircle
			(feet.position, groundCheckRadius, groundLayer);
		
		float move = Input.GetAxis("Horizontal");
		this.GetComponent<Rigidbody2D>().velocity =
			new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
	}
}
