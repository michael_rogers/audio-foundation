﻿using UnityEngine;
using System.Collections;

public class HandleMicrophoneScripts : MonoBehaviour {
	AudioClip ac;
	// Use this for initialization
	void Start () {
		Debug.Log (Microphone.devices[0]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void record(){
		ac = Microphone.Start ("Built-In Microphone",false,3,44100);
	}

	public void stop(){

	}

	public void play(){
		AudioSource.PlayClipAtPoint (ac,new Vector3(0f,0f,0f));
	}
}
